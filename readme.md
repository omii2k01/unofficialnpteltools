# Unofficial NPTEL Tools Project

## Overview

This is a set of Tools which work with NPTEL and its various services

## Features

[X]  Allows downloading Certificates in maximum resolution given a URL, a folder containing certificates that might need verification or a single file that you want to get in full resolution.


## Project Status

The NPTEL Tools Project is currently a work-in-progress (WIP).

## Contact

For inquiries and feedback, please reach out to [omii2k01@gmail.com](mailto:omii2k01@gmail.com).

---

Thank you for your interest in the Unofficial NPTEL Tools Project. We look forward to bringing more updates and enhancements to this project in the near future!